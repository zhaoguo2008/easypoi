package org.jeecgframework.poi.excel.entity.vo;

/**
 * 正常导出Excel
 * @Author JueYue on 14-3-8.
 * 静态常量
 */
public interface MapExcelConstants extends BasePOIConstants {
    /**
     * 单Sheet导出
     */
    public final static String JEECG_MAP_EXCEL_VIEW = "jeecgMapExcelView";
    /**
     * Entity List
     */
    public final static String ENTITY_LIST          = "data";
    /**
     * 数据列表
     */
    public final static String MAP_LIST             = "mapList";

}
